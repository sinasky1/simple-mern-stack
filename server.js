const mongoose = require("mongoose");
const express = require("express");
const app = express();
const bodyparser = require("body-parser");
const cors = require("cors");
const router = express.Router();
dbURI = "mongodb://127.0.0.1:27017";
app.use(cors());
app.use(bodyparser());
mongoose
  .connect(dbURI)
  .then(() => {
    console.log("DB Connected");
  })
  .catch((err) => {
    if (err) throw err;
  });
app.use("/cars", router);
const mySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  _id: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

const Car = new mongoose.model("cars", mySchema);

router.route("/").get((req, res) => {
  Car.find({}).then((data) => res.json(data));
});
router.route("/add").post((req, res) => {
  mycar = new Car({
    name: req.body.name,
    _id: req.body._id,
  });
  mycar.save();
});
router.route("/").post((req, res) => {
  Car.deleteOne({ _id: req.body.id }).then(() => {
    console.log("deleted!");
  });
});

router.route("/update").post((req, res) => {
  updatedName = req.body.name;

  id = req.body._id;
  Car.updateOne({ _id: id }, { name: updatedName }, (err, res) => {
    if (err) throw err;
  });
});
port = 5000 || process.env.PORT;
app.listen(port, () => {
  console.log(`listening on port:${port}`);
});
