import React, { Component } from "react";
import { v4 as uuid } from "uuid";
import axios from "axios";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { cars: [], nameInput: "" };
  }
  componentDidMount() {
    axios.get("http://localhost:5000/cars").then((res) => {
      this.setState({ cars: res.data });
    });
  }
  onChangeHandler = (e) => {
    const input = e.currentTarget.value;
    this.setState({ nameInput: input });
  };
  sendHandler = (e) => {
    e.preventDefault();
    const car = {
      name: this.state.nameInput,
      _id: uuid(),
    };
    axios
      .post("http://localhost:5000/cars/add", car)
      .then((result) => console.log(result));
    const cars = [...this.state.cars];
    cars.push(car);
    console.log(cars);
    this.setState({ cars });
  };
  deleteHandler = (e) => {
    e.preventDefault();
    const currentId = e.currentTarget.id;
    console.log(currentId);
    axios.post("http://localhost:5000/cars/", { id: currentId });

    const cars = [...this.state.cars];
    const filterdCars = cars.filter((c) => {
      return c._id != currentId;
    });
    this.setState({
      cars: filterdCars,
    });
  };
  editHandler = (e) => {
    e.preventDefault();
    const currentId = e.currentTarget.id;

    const index = this.state.cars.findIndex((c) => {
      return c._id === currentId;
    });
    const cars = [...this.state.cars];
    cars[index].name = this.state.nameInput;
    this.setState({ cars });

    axios.post("http://localhost:5000/cars/update/", cars[index]);
  };
  render() {
    return (
      <div className="App">
        <h1 className="hello">Hello world</h1>
        <form>
          <label>name:</label>
          <input onChange={this.onChangeHandler}></input>
        </form>
        <button className="mb btn-add" onClick={this.sendHandler}>
          Add
        </button>
        {this.state.cars.map((car) => {
          return (
            <div key={car._id} className="border-item">
              <p>{car.name}</p>
              <button
                id={car._id}
                onClick={this.deleteHandler}
                className="btn-delete"
              >
                Delete
              </button>
              <button
                id={car._id}
                className="btn-edit"
                onClick={this.editHandler}
              >
                Edit
              </button>
            </div>
          );
        })}
      </div>
    );
  }
}

export default App;
